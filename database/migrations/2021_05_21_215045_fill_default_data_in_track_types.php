<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
/**
 * Заполнение таблицы типы треков данными по умолчанию.
 * Class CreateTracksTable.
 */
class FillDefaultDataInTrackTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $data = [
            [
                'name'       => 'Бесполезный трек',
                'code'       => 'useless',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'Неопределенный трек',
                'code'       => 'undefined',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'       => 'Полезный трек',
                'code'       => 'useful',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        foreach ($data as $datum) {
            DB::table('track_types')->insert($datum);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {

    }
}
