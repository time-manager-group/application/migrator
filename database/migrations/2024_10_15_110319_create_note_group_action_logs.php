<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoteGroupActionLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('note_group_action_logs', function (Blueprint $table) {
            $table->timestamp('time');
            $table->uuid('request_id');
            $table->char('action');
            $table->uuid('note_id');
            $table->uuid('group_id')->nullable();
            $table->uuid('row_id')->nullable();
            $table->jsonb('params');
            $table->jsonb('payload');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('note_group_action_logs');
    }
}
