<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoteGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('note_groups', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->char('name');
            $table->timestamps();
            $table->jsonb('meta');
            $table->jsonb('row_items');
        });
        Schema::table('note_groups', function (Blueprint $table) {
            $table->uuid('note_id');
            $table->foreign('note_id')->references('id')->on('notes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('note_groups', function (Blueprint $table) {
            $table->dropForeign('note_groups_note_id_foreign');
        });
        Schema::dropIfExists('note_groups');
    }
}
