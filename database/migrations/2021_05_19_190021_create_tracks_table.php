<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Таблица с треками времени.
 * Class CreateTracksTable.
 */
class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->char('name');
            $table->dateTime('date_start');
            $table->dateTime('date_end')
                ->nullable();
            $table->boolean('is_active')
                ->default(false);
        });

        Schema::table('tracks', function (Blueprint $table) {
            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('track_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('tracks', function (Blueprint $table) {
            $table->dropForeign('tracks_type_id_foreign');
        });

        Schema::dropIfExists('tracks');
    }
}
