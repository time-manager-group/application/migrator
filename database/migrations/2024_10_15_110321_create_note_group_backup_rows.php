<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoteGroupBackupRows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('note_group_backup_rows', function (Blueprint $table) {
            $table->timestamp('time');
            $table->uuid('note_id');
            $table->uuid('group_id');
            $table->uuid('row_id');
            $table->char('row_name');
            $table->jsonb('meta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('note_group_backup_rows');
    }
}
