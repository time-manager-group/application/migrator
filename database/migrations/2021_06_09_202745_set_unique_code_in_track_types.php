<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Сделать уникальную колонку code в таблице типам треков.
 * Class SetUniqueCodeInTrackTypes.
 */
class SetUniqueCodeInTrackTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('track_types', function (Blueprint $table) {
            $table->unique('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('track_types', function (Blueprint $table) {
            $table->dropUnique('track_types_code_unique');
        });
    }
}
